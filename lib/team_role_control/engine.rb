require 'pg'
require 'milligram'

module TeamRoleControl
  ::TRC = TeamRoleControl

  class Engine < ::Rails::Engine
    isolate_namespace TeamRoleControl

    config.generators do |g| 
      g.template_engine :haml
    end

    config.after_initialize do |g|
      TeamRoleControl.configure do end unless TeamRoleControl.configuration.present?

      if TeamRoleControl.configuration.ignore_teams && !TeamRoleControl::Team.count
        TeamRoleControl::Team.create(
          name: TeamRoleControl.configuration.exclusive_team_name,
          description: "All roles will be associated to this team which should be hidden from the frontend."
        )
      end
    end

  end
end
