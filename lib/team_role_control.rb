require "team_role_control/engine"
require "haml"
require 'haml-rails'
require 'coffee-rails'
require 'jquery-rails'
require 'select2-rails'
require 'cocoon'
require 'pg'
require 'pg_search'
require 'active_record'
require 'material_icons'

module TeamRoleControl
  mattr_accessor :configuration

  def self.configure
    self.configuration ||= Configuration.new
    yield(configuration)
  end

  class Configuration
    attr_accessor :ignore_teams, :user_display_name_method

    def initialize
      @ignore_teams = false
      @user_display_name_method = :full_name
    end

  end
end
