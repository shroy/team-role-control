class @TRC.UserTeamRolesForm
  constructor: (el) ->
    @el =
      parent: $(el)
      roles: $(el).find '.js--roles'
      role_permissions: $(el).find '.js--role-permissions'
      additional_permissions: $(el).find '.js--additional-permissions'

    @team_id = @el.parent.data 'team-id'

    @setupSearch()
    @toggleAdditionPermissions()

    @el.parent.on 'change', '.trc-Checkbox--tab', (e) =>
      $role = $(e.currentTarget)

      @selected_role =
        $el: $role
        path: $role.data 'team-permissions-path'
        check_box: $role.find(".-filter-:checkbox").first()

      @getPermissions()

    @el.additional_permissions.on 'click', '.trc-Checkbox', (e) =>
      e.preventDefault()
      if $(e.target).hasClass 'material-icons'
        $(e.currentTarget).remove()

  setupSearch: ->
    @search = new TRC.Search @el.parent.find(".trc-Search").first(), @
    @search.formatResults = (item) ->
      { id: item.id, text: item.display_name }
    @search.onSelect = (selected) =>
      @appendSelected selected
      @search.box.val('').trigger('change')

  computedParams: (params) =>
    params['role_ids'] = @get_active_roles()
    params['exclude_ids'] = @get_active_additional_ids()
    return params

  get_active_roles: -> 
    @el.roles.find(".-filter-:checkbox:checked").map(-> parseInt(@value) ).get()

  get_active_additional_ids: ->
    @el.additional_permissions.find('.trc-Checkbox').map(-> @getAttribute('data-id')).get()

  getPermissions: ->
    active_roles = @get_active_roles()

    role_ids = active_roles.filter (role) =>
      role isnt parseInt(@selected_role.check_box.val())

    $.ajax
      url: @selected_role.path
      dataType: 'json'
      data:
        role_ids: role_ids 
      success: @filterPermissions

  filterPermissions: (data, status, xhr) =>
    template = data.template

    if @selected_role.check_box.is(':checked')
      @el.role_permissions.append template 
    else
      $html = $('<div/>').html('<div>' + template + '</div>').contents()
      $html.find('.trc-Checkbox').each (i, el) =>
        id = $(el).data('id')
        @el.role_permissions.find(".trc-Checkbox[data-id=#{id}]").remove()

    @toggleAdditionPermissions()

  appendSelected: (selected) ->
    check_box = "
      <div class='trc-Checkbox u-noMarginX' data-id='#{selected.id}'>
        <label for='user_special_permission_override_ids_#{selected.id}'><input type='checkbox' value='#{selected.id}' checked='checked' name='user[special_permission_override_ids][]' id='user_special_permission_override_ids_#{selected.id}'>
        <div class='trc-Checkbox-cover'>
          <i class='material-icons'>close</i>
        #{selected.text}
        </div>
        </label>
      </div>
    "
    @el.additional_permissions.find('.js--additional-permission-container').append check_box

  toggleAdditionPermissions: ->
    if @get_active_roles().length
      @el.parent.find(".trc-Search").next('.select2').show()
      @el.role_permissions.find('.js--message').hide()
      # Hide Additional permission if exists in Role Permissions list.
      _permission_ids = @el.role_permissions.find('.trc-Checkbox').map(-> parseInt @getAttribute 'data-id').get()
      @el.additional_permissions.find(".trc-Checkbox").each ->
        hide = _permission_ids.some (id) => 
          id is parseInt(@getAttribute('data-id'))
        $(@).find('input').prop('checked', !hide)
    else
      @el.role_permissions.find('.js--message').show()
      @el.additional_permissions.find(".trc-Checkbox input").prop('checked', false)
      @el.parent.find(".trc-Search").next('.select2').hide()


@TRC.UserSettingsForm = ->
  teams = $("#js-UserSettings").find '.js--team'
  for team in teams
    new TRC.UserTeamRolesForm team



