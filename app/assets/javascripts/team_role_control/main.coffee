@TRC or= {}

@TRC.typeIsArray = Array.isArray || ( value ) -> return {}.toString.call( value ) is '[object Array]'

$(document).on 'ready', ->
  $trcLayout = $('.trc-Layout')

  if $trcLayout.length and $trcLayout.data('controller-action') is 'users#edit'
    TRC.UserSettingsForm()

  if $trcLayout.length and $trcLayout.data('controller-action') is 'roles#edit' or $trcLayout.data('controller-action') is 'roles#new'
    new TRC.RoleForm