class @TRC.Search
  constructor: (box, parent) ->
    @box = $(box)
    @placeholder = "Search..."
    @placeholder or= @box.data 'placeholder'
    @path = @box.data 'path'

    @box.select2
      placeholder: @placeholder
      escapeMarkup: (m) -> m
      minimumInputLength: 2
      ajax:
        url: @path
        dataType: 'json'
        delay: 500
        data: parent.computedParams
        processResults: (data) =>
          results: data.results.map @formatResults

    @box.on 'select2:select', (e) =>
      @onSelect e.params.data

  # computedParams: ->
  #   console.log "@CP"

  formatResults: (item) ->
    keys = Object.keys(item)
    keys.splice(keys.indexOf("id"), 1)

    return { id: item.id, text: item[keys[0]] }

  onSelect: (selected) ->
    console.log selected

