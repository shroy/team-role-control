class @TRC.RoleForm

  constructor: ->
    @el = $("#js--RoleForm")
    @search = new TRC.Search @el.find(".trc-Search").first(), @

    @search.formatResults = (item) ->
      { id: item.id, text: item.display_name }
    @search.onSelect = (selected) =>
      @appendSelected selected
      @search.box.val('').trigger('change')

    @el.find('.js--permission-container').on 'click', '.trc-Checkbox', (e) =>
      e.preventDefault()
      if $(e.target).hasClass 'material-icons'
        $(e.currentTarget).remove()

  computedParams: (params) =>
    params['exclude_ids'] = @el.find('.js--permission-container').find('.trc-Checkbox').map(-> @getAttribute('data-id')).get()
    return params

  appendSelected: (selected) ->
    check_box = "
      <div data-id='#{selected.id}' class='trc-Checkbox'>
        <label for='role_team_permission_ids_#{selected.id}'>
          <input type='checkbox' value='#{selected.id}' checked='checked' name='role[team_permission_ids][]' id='role_team_permission_ids_#{selected.id}'> 
          <div class='trc-Checkbox-cover'>
              <i class='material-icons'>close</i>
              #{selected.text}
          </div>
        </label>
      </div>
    "

    @el.find('.js--permission-container').append check_box