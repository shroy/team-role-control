# frozen_string_literal: true

module TeamRoleControl
  module Permittable
    extend ActiveSupport::Concern
    # TODO: look at this

    # module ClassMethods
    def is_permissible?(user: current_user, team: current_user.teams, action: action_name, object: get_controllers_model)
      team_permission = user.team_permissions.joins(:permission).where(
        team: team,
        team_role_control_permissions: {
          object: object,
          action: action
        }
      )

      override = TRC::Override.find_by(team_permission: team_permission, user: user)

      if override
        return override.is_permitted
      else
        return team_permission.any?
      end
    end

    private

    def get_controllers_model
      respond_to?(:controller_path) && controller_path.classify
    end
    # end
  end
end
