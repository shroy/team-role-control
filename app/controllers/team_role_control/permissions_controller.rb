require_dependency "team_role_control/application_controller"

module TeamRoleControl
  class PermissionsController < ApplicationController

    def index
      @permissions = Permission.all

      if params[:term]
        @permissions = {
          results: Permission.search( params[:term] )
        }
      end

      respond_to do |format|
        format.json { render json: @permissions }
      end
    end

  end
end