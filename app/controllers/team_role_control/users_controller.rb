require_dependency "team_role_control/application_controller"

# Currently there no support for User creation since users are added to the database via a third party source.

module TeamRoleControl
  class UsersController < ::TeamRoleControl::ApplicationController
    before_action :set_user, only: [:show, :edit, :update, :destroy]

    # GET /users
    def index
      @users = ::User.all
    end

    # GET /users/1
    def show
    end

    # # GET /users/new
    # def new
    #   @user = ::User.new
    # end

    # GET /users/1/edit
    # Can only currently edit Role and Team Settings
    def edit
    end

    # # POST /users
    # def create
    #   @user = ::User.new(user_params)

    #   if @user.save
    #     redirect_to @user, notice: 'User was successfully created.'
    #   else
    #     render :new
    #   end
    # end

    # PATCH/PUT /users/1
    def update
      if user_update_transaction
        redirect_to user_path(@user), notice: "User was successfully updated."
      else
        render :edit
      end
    end

    # # DELETE /users/1
    # def destroy
    #   @user.destroy
    #   redirect_to users_url, notice: 'User was successfully destroyed.'
    # end

    private

    def user_update_transaction
      updated = false

      ActiveRecord::Base.transaction do
        @user.overrides.destroy_all
        updated = @user.update(user_params)
      end
      return updated
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = ::User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(
        role_ids: [],
        special_permission_override_ids: [],
        revoked_permission_override_ids: [],
      )
    end
  end
end
