module TeamRoleControl
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    layout "team_role_control/main"

    def ignoring_teams
      TeamRoleControl.configuration.ignore_teams
    end

    helper_method :ignoring_teams

  end
end
