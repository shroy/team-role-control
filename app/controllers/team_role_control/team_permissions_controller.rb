require_dependency "team_role_control/application_controller"

module TeamRoleControl
  class TeamPermissionsController < ApplicationController
    before_action :set_roles_team_permission_ids

    def index
      @role = Role.find params[:role_id]
      team_permissions = @role.team_permissions.where.not(id: @roles_tp_ids)

      @html_string = render_to_string( 
        partial: "team_role_control/users/permissions_check_boxes",
        locals: {
          object: :user, 
          method: "revoked_permission_override_ids", 
          collection: team_permissions,
          is_new: true
        }
      )

      render json: { template: @html_string }
    end


    def search
      @team_permissions = {
        results: TeamPermission.where(team_id: params[:team_id]).search( params[:term] )
      }

      if params[:role_ids] || params[:exclude_ids]
        exclude_ids = params[:exclude_ids] || []
        all_excluded_ids = (exclude_ids + @roles_tp_ids)
        @team_permissions[:results] = @team_permissions[:results].where.not(id: all_excluded_ids)
      end

      render json: @team_permissions, methods: :display_name
    end


    private


    def set_roles_team_permission_ids
      @roles_tp_ids = Role.where(id: params[:role_ids]).map { |role| role.team_permissions.pluck(:id) }.flatten || []
    end

  end
end