require_dependency "team_role_control/application_controller"

module TeamRoleControl
  class RolesController < ApplicationController
    before_action :set_team
    before_action :set_role, only: [:show, :edit, :update, :destroy]

    # GET team/:team_id/roles
    def index
      redirect_to team_path(@team)
    end

    # GET team/:team_id/roles/1
    def show
    end

    # GET team/:team_id/roles/new
    def new
      @role = @team.roles.build
    end

    # GET team/:team_id/roles/1/edit
    def edit
    end

    # POST team/:team_id/roles
    def create
      @role = @team.roles.build(role_params)

      if @role.save
        redirect_to @team, notice: 'Role was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT team/:team_id/roles/1
    def update
      if @role.update(role_params)
        redirect_to @team, notice: 'Role was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE team/:team_id/roles/1
    def destroy
      @role.destroy
      redirect_to @team, notice: 'Role was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_team
        @team = ignoring_teams ? Team.first : Team.find(params[:team_id])
      end

      def set_role
          @role = @team.roles.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def role_params
        params.require(:role).permit(:name, :description, team_permission_ids: [])
      end
  end
end
