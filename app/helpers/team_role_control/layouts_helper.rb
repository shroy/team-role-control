module TeamRoleControl
  module LayoutsHelper

    def inside_layout(layout = "application", &block)
      render inline: capture(&block), layout: "layouts/#{layout}"
    end

    def active_tab? tab_name
      if ignoring_teams && (controller_name === 'teams')
        namespace = 'roles'
      elsif !ignoring_teams && (controller_name === 'roles')
        namespace = 'teams'
      else
        namespace = controller_name
      end
      
      tab_name === namespace ? 'is-active' : ''
    end

    def trc_header **locals, &haml
      if block_given?
        render layout: 'team_role_control/shared/header', locals: locals, &haml
      else
        render partial: 'team_role_control/shared/header', locals: locals
      end
    end
    
  end
end