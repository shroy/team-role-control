module TeamRoleControl
  module ApplicationHelper
    include ::ApplicationHelper

    ICON_NAME = {
      teams: 'group',
      users: 'contacts',
      roles: 'transfer_within_a_station',
      permissions: 'vpn_key',
      edit: 'edit'
    }

    def material_icon type
      icon_name = ICON_NAME.key?(type) ? ICON_NAME[type.to_sym] : type.to_s
      content_tag(:i, icon_name, class: 'material-icons')
    end
  end
end
