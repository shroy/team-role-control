# frozen_string_literal: true

module TeamRoleControl
  class Team < ApplicationRecord
    has_many :roles, dependent: :destroy
    has_many :users, through: :roles
    has_many :team_permissions, dependent: :destroy

    accepts_nested_attributes_for :roles, allow_destroy: true

    validates :name, presence: true
  end
end
