# frozen_string_literal: true

module TeamRoleControl
  class RoleTeamPermission < ApplicationRecord
    belongs_to :role
    belongs_to :team_permission
  end
end
