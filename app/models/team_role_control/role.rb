# frozen_string_literal: true

module TeamRoleControl
  class Role < ApplicationRecord
    has_many :user_roles, dependent: :destroy
    has_many :users, through: :user_roles

    belongs_to :team
    has_many :role_team_permissions, dependent: :destroy
    has_many :team_permissions, through: :role_team_permissions
    has_many :permissions, through: :team_permissions

    validates :name, presence: true

    def display_name
      "#{name} - #{team.name}"
    end
  end
end
