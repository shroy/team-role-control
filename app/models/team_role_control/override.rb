# frozen_string_literal: true

module TeamRoleControl
  class Override < ApplicationRecord
    belongs_to :team_permission
    belongs_to :user

    validates :team_permission_id, uniqueness: { scope: :user_id }

    scope :special, -> { where(is_permitted: true) }
    scope :revoked, -> { where(is_permitted: false) }
  end
end
