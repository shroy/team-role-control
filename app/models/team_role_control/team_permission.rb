# frozen_string_literal: true

module TeamRoleControl
  class TeamPermission < ApplicationRecord
    include PgSearch::Model
    pg_search_scope :search, associated_against: {
               permission: [:object, :action],
               team: :name,
             }

    belongs_to :team
    belongs_to :permission

    has_many :role_team_permissions
    has_many :roles, through: :role_team_permissions
    has_many :users, through: :roles, inverse_of: :team_permissions

    has_many :overrides, dependent: :destroy

    def display_name
      "#{permission.action.capitalize} #{permission.object}"
    end

    def display_name_with_team
      "#{display_name} for #{team.name}"
    end

    def self.create_for_all_teams(permission)
      Team.all.each do |team|
        create!(
          permission: permission,
          team: team,
        )
      end
    end
  end
end
