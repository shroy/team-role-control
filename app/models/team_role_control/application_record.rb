# frozen_string_literal: true

module TeamRoleControl
  class ApplicationRecord < ActiveRecord::Base
    self.abstract_class = true
  end
end
