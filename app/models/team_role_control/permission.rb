# frozen_string_literal: true

module TeamRoleControl
  class Permission < ApplicationRecord
    include PgSearch::Model
    pg_search_scope :search, against: [:action, :object]

    has_many :team_permissions, dependent: :delete_all
    has_many :users, through: :team_permissions, inverse_of: :permissions
    has_many :roles, -> { distinct }, through: :team_permissions
    validates :object, uniqueness: { scope: :action, message: "Object/Action permission already exists!" }

    after_save :create_team_permissions

    private

    def create_team_permissions
      TeamPermission.create_for_all_teams(self)
    end
  end
end
