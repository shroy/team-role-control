# frozen_string_literal: true

module TeamRoleControl
  module Support
    extend ActiveSupport::Concern

    included do
      def has_team_role?(team, role)
        roles.joins(:team).where("team_role_control_teams.name = '?' AND team_role_control_roles.name = '?'", team, role).any?
      end
    end

    module ClassMethods
      def team_role_appointees(*_args)
        # self.send :include, TeamRoleControl::UserQueries

        has_many :user_roles, class_name: '::TRC::UserRole'
        has_many :roles, through: :user_roles, class_name: '::TRC::Role'

        has_many :teams, -> { distinct }, through: :roles, class_name: '::TRC::Team'
        has_many :team_permissions, -> { distinct }, through: :roles, class_name: '::TRC::TeamPermission'
        has_many :permissions, -> { distinct }, through: :team_permissions, class_name: '::TRC::Permission'

        has_many :overrides, class_name: '::TRC::Override'
        has_many :special_permissions, -> { special }, class_name: '::TRC::Override'
        has_many :revoked_permissions, -> { revoked }, class_name: '::TRC::Override'

        has_many :overriden_team_permissions, through: :overrides, source: :team_permission, class_name: '::TRC::TeamPermission'
        has_many :overriden_teams, through: :overriden_team_permissions, source: :team, class_name: '::TRC::Team'

        has_many :special_permission_overrides,
                 through: :special_permissions,
                 source: :team_permission,
                 class_name: '::TRC::TeamPermission'

        has_many :revoked_permission_overrides,
                 through: :revoked_permissions,
                 source: :team_permission,
                 class_name: '::TRC::TeamPermission'
      end

      def assigned_to_team
        belongs_to :team, class_name: '::TRC::Team'
      end
    end
  end

  # module UserQueries
  #   def all_permissions
  #     # If they have the team permission through the role and the override is_permitted, then remove redundancy.
  #     # CURRENTLY NOT WORKING CORRECTLY
  #     overrides.where.not(
  #       id: team_permissions.pluck(:id),
  #       is_permitted: true
  #     )
  #   end
  # end
end
