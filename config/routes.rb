TeamRoleControl::Engine.routes.draw do
  root to: "users#index"
  resources :roles
  resources :teams
  resources :users, except: [:new, :create, :destroy]
  resources :permissions, only: [:index]

  resources :team_permissions, only: [] do
    get 'search', on: :collection
  end

  resources :teams, as: :team, only: [] do
    resources :team_permissions, only: [] do
      get 'search', on: :collection
    end
    resources :roles do
      resources :team_permissions, only: [:index]
    end
  end


end
