$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "team_role_control/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name = "team_role_control"
  s.version = TeamRoleControl::VERSION
  s.authors = ["Joshua Shroy"]
  s.email = ["joshshroy@gmail.com"]
  s.homepage = "http://www.nowhere.meh"
  s.summary = "User and team management and permissions system."
  s.description = "It does all the user management things."
  s.license = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  s.test_files = Dir["spec/**/*"]

  s.add_dependency "rails", ">= 5.1"
  s.add_dependency "haml"
  s.add_dependency "haml-rails"
  s.add_dependency "sass-rails", ">= 3.2"
  s.add_dependency "coffee-rails"
  s.add_dependency "jquery-rails"

  s.add_dependency "devise"
  s.add_dependency "milligram"
  s.add_dependency "material_icons"
  s.add_dependency "select2-rails"
  s.add_dependency "pg_search", "~> 2.3"
  s.add_dependency "cocoon", " ~> 1.2"

  s.add_development_dependency "better_errors"
  s.add_development_dependency "binding_of_caller"
  s.add_development_dependency "pry"

  s.add_development_dependency "pg", "0.21.0"
  s.add_development_dependency "rspec"
  s.add_development_dependency "rspec-rails", "~> 3.7"
  s.add_development_dependency "factory_bot_rails", "~> 4.8"
  s.add_development_dependency "faker"
  s.add_development_dependency "sass-rails", ">= 3.2"
end
