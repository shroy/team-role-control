class CreateTeamRoleControlTeams < ActiveRecord::Migration[5.1]
  def change
    create_table :team_role_control_teams do |t|
      t.string :name, null: false
      t.text :description

      t.timestamps
    end
  end

end
