class CreateTeamRoleControlRoles < ActiveRecord::Migration[5.1]
  def change
    create_table :team_role_control_roles do |t|
      t.string :name, null: false
      t.text :description
      t.integer :team_id, index: true, foreign_key: true
      
      t.timestamps
    end
  end
end
