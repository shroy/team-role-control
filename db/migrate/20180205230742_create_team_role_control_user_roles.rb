class CreateTeamRoleControlUserRoles < ActiveRecord::Migration[5.1]
  def change
    create_table :team_role_control_user_roles do |t|
      t.integer :user_id, index: true, foreign_key: true
      t.integer :role_id, index: true, foreign_key: true

      t.timestamps
    end
  end
end
