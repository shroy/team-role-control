class CreateTeamRoleControlRoleTeamPermissions < ActiveRecord::Migration[5.1]
  def change
    create_table :team_role_control_role_team_permissions do |t|
      t.integer :role_id
      t.integer :team_permission_id

      t.timestamps
    end
  end
end
