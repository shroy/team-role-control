class CreateTeamRoleControlTeamPermissions < ActiveRecord::Migration[5.1]
  def change
    create_table :team_role_control_team_permissions, comment: "A Permission within the scope of a Team" do |t|
      t.integer :permission_id, index: true, foreign_key: true
      t.integer :team_id, index: true, foreign_key: true

      t.timestamps
    end
  end
end
