class CreateTeamRoleControlOverrides < ActiveRecord::Migration[5.1]
  def change
    create_table :team_role_control_overrides do |t|
      t.integer :user_id, index: true, foreign_key: true
      t.integer :team_permission_id, index: true, foreign_key: true
      t.boolean :is_permitted, default: false

      t.timestamps
    end
  end
end
