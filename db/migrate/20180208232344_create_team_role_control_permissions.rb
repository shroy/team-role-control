class CreateTeamRoleControlPermissions < ActiveRecord::Migration[5.1]
  def change
    create_table :team_role_control_permissions do |t|
      t.string :action, null: false
      t.string :object, null: false
      
      t.timestamps
    end
  end
end
