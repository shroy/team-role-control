class AddTrcForeignKeys < ActiveRecord::Migration[5.1]
  def change
    change_column :team_role_control_overrides, :user_id, :bigint
    change_column :team_role_control_overrides, :team_permission_id, :bigint

    add_foreign_key :team_role_control_overrides, :users
    add_foreign_key :team_role_control_overrides, :team_role_control_team_permissions, column: :team_permission_id

    add_foreign_key :team_role_control_role_team_permissions, :team_role_control_team_permissions, column: :team_permission_id
    add_foreign_key :team_role_control_role_team_permissions, :team_role_control_roles, column: :role_id

    add_foreign_key :team_role_control_roles, :team_role_control_teams, column: :team_id

    add_foreign_key :team_role_control_team_permissions, :team_role_control_permissions, column: :permission_id
    add_foreign_key :team_role_control_team_permissions, :team_role_control_teams, column: :team_id

    add_foreign_key :team_role_control_user_roles, :users
    add_foreign_key :team_role_control_user_roles, :team_role_control_roles, column: :role_id
  end
end
