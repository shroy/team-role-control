# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180720002754) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "blogs", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_blogs_on_user_id"
  end

  create_table "team_role_control_overrides", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "team_permission_id"
    t.boolean "is_permitted", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["team_permission_id"], name: "index_team_role_control_overrides_on_team_permission_id"
    t.index ["user_id"], name: "index_team_role_control_overrides_on_user_id"
  end

  create_table "team_role_control_permissions", force: :cascade do |t|
    t.string "action", null: false
    t.string "object", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "team_role_control_role_team_permissions", force: :cascade do |t|
    t.integer "role_id"
    t.integer "team_permission_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "team_role_control_roles", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.integer "team_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["team_id"], name: "index_team_role_control_roles_on_team_id"
  end

  create_table "team_role_control_team_permissions", force: :cascade, comment: "A Permission within the scope of a Team" do |t|
    t.integer "permission_id"
    t.integer "team_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["permission_id"], name: "index_team_role_control_team_permissions_on_permission_id"
    t.index ["team_id"], name: "index_team_role_control_team_permissions_on_team_id"
  end

  create_table "team_role_control_teams", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "team_role_control_user_roles", force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role_id"], name: "index_team_role_control_user_roles_on_role_id"
    t.index ["user_id"], name: "index_team_role_control_user_roles_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "blogs", "users"
  add_foreign_key "team_role_control_overrides", "team_role_control_team_permissions", column: "team_permission_id"
  add_foreign_key "team_role_control_overrides", "users"
  add_foreign_key "team_role_control_role_team_permissions", "team_role_control_roles", column: "role_id"
  add_foreign_key "team_role_control_role_team_permissions", "team_role_control_team_permissions", column: "team_permission_id"
  add_foreign_key "team_role_control_roles", "team_role_control_teams", column: "team_id"
  add_foreign_key "team_role_control_team_permissions", "team_role_control_permissions", column: "permission_id"
  add_foreign_key "team_role_control_team_permissions", "team_role_control_teams", column: "team_id"
  add_foreign_key "team_role_control_user_roles", "team_role_control_roles", column: "role_id"
  add_foreign_key "team_role_control_user_roles", "users"
end
