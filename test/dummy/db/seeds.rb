16.times do |index|
  password = Faker::Internet.password(8, 20, true)
  
  User.create!(
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    email: Faker::Internet.safe_email,
    password: password,
    password_confirmation: password
  )
end

3.times do |index|
  team = TeamRoleControl::Team.create!(
    name: Faker::Team.name.capitalize,
    description: Faker::Hipster.sentence
  )
  rand(1...4).times do |index|
    role = TeamRoleControl::Role.new(
      name: Faker::Company.profession.capitalize,
      description: Faker::Company.catch_phrase,
      team: team
    )
    role.users << User.all.sample(rand(1...3))
    role.save!
  end

  # rand(1...total_prms).times do |index|
  #   tp = TeamRoleControl::TeamPermission.new(
  #     team: team,
  #     permission: TeamRoleControl::Permission.find(rand(1...total_prms))
  #   )
  #   p ('-' * 10) + ' ' + team.roles.count.to_s + ' ' + ('-' * 10)
    # team.team_permissions.roles = team.roles.sample(rand(1...team.roles.count)) if team.roles.count
  #   tp.save!
  # end
end

['index', 'show', 'new', 'edit', 'create', 'update', 'destroy'].each do |action|
  TeamRoleControl::Permission.create!(
    action: action,
    object: 'Blog'
  )
end

total_prms = TeamRoleControl::Permission.count
