require_relative 'boot'

require 'rails/all'
require 'devise'

Bundler.require(*Rails.groups)
require "team_role_control"

module Dummy
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1
    config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
