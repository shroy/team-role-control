# TeamRoleControl
Short description and motivation.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'team_role_control'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install team_role_control
```

Require the appropriate assets..
```css
*= require team_role_control/application
or
@import "team_role_control/application";
```
```js
//= require team_role_control/application
``` 

In your routes.rb:
```ruby
  mount ZenSupport::Engine, at: "/trc"
```

## Initializer

Install the configuration initializer:
```bash
$ rails g team_role_control:install
```

Copy over database migrations:
```bash
$ rake team_role_control:install:migrations
```

Note: This will create migrations for Rails 5.1. If you are using 4.2 or below, you will want to remove the `[5.1]` from the inherited `ActiveRecord::Migration` in the migration files.


Run:
```bash
$ rake db:migrate
```

That will run the necessary migrations for creating your Permission, Team, and Role's tables.

## Team Role Appointees
Adding the following class method to your User table will include the necessary relationships for appointing roles to users, adding them to teams, and allowing them to have permission overrides.

```
class User < ApplicationRecord
 team_role_appointees

 def full_name
  "#{first_name} #{last_name}"
 end
end
```

Note your User table is expected to provide a full_name method.


## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

